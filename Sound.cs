﻿namespace AmbientSFX
{
    class Sound
    {
        private string _name;
        private string _path;

        public Sound(string path)
        {
            _name = path.Substring(9);
            _path = path;
        }

        public string getName()
        {
            return _name;
        }

        public string getPath()
        {
            return @_path;
        }
    }
}
