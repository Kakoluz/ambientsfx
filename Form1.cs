﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using WMPLib;

namespace AmbientSFX
{
    public partial class Form1 : Form
    {
        List<Sound> sounds = new List<Sound>();
        int current = 0;
        Random n = new Random();
        WindowsMediaPlayer player = new WindowsMediaPlayer();
        Thread cowThread;
        Mutex cowMutex = new Mutex();
        int maxTime = 5;

        private void back_Sound(Object obj)
        {
            Random time = new Random();
            Sound sound = (Sound) obj;
            System.Media.SoundPlayer cow = new System.Media.SoundPlayer(sound.getPath());
            while (true)
            {
                cowMutex.WaitOne();
                int value = maxTime;
                cowMutex.ReleaseMutex();

                int waittime = time.Next(0, value);
                Thread.Sleep(time.Next(0, 59 * waittime) * 1000);
                cow.Play();
                Thread.Sleep(59 * (value - waittime) * 1000);
            }
        }

        private void load_sounds()
        {
            string[] files = null;
            if (System.IO.Directory.Exists(@"./sounds"))
                files = System.IO.Directory.GetFiles(@"./sounds");
            else
                System.IO.Directory.CreateDirectory(@"./sounds");
            if (files.Length != 0 || files == null)
            {
                foreach (string file in files)
                {
                    sounds.Add(new Sound(file));
                }
                listBox1.BeginUpdate();
                foreach (Sound sound in sounds)
                {
                    listBox1.Items.Add(sound.getName());
                }
                listBox1.EndUpdate();
                listBox1.SetSelected(current, true);
            }
        }

        public Form1()
        {
            cowThread = new Thread(back_Sound);
            InitializeComponent();
            load_sounds();
            textBox1.Text = maxTime.ToString();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cowThread.Abort();
            player.close();
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (current - 1 >= 0)
            {
                current -= 1;
                listBox1.SetSelected(current, true);
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (current >= 0 && current < sounds.Count)
            {
                if (player.URL != sounds[current].getPath())
                    player.URL = sounds[current].getPath();
                player.controls.play();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (current + 1 < sounds.Count)
            {
                current += 1;
                listBox1.SetSelected(current, true);
            }
        }

        private void reproducirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cowThread.Abort();
            cowThread = new Thread(back_Sound);
            cowThread.Start(new Sound(@"sounds/Cow.wav"));
        }

        private void pausarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cowThread.Abort();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            current = listBox1.SelectedIndex;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            cowThread.Abort();
            player.close();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
                button1.Click += new EventHandler(this.button1_Click);
        }

        private void recargarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sounds.Clear();
            listBox1.Items.Clear();
            load_sounds();
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (!Int32.TryParse(textBox1.Text, out maxTime))
            {
                MessageBoxEx.Show(this, "No es un número", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBox1.Text = maxTime.ToString();
            }
            else
                textBox1.Text = maxTime.ToString();
        }
    }
}
